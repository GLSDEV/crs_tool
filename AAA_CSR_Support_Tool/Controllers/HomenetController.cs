﻿using AAA_CSR_Support_Tool.BusinessLayer;
using AAA_CSR_Support_Tool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAA_CSR_Support_Tool.Controllers
{
    public class HomenetController : Controller
    {

        private readonly IDataAccess _dataAccess;

        public HomenetController(IDataAccess da)
        {
            _dataAccess = da;
        }

        // GET: Homenet
        public ActionResult Index()
        {
            var homenetDealers = _dataAccess.GetAllHomenetDealers();

            return View(homenetDealers);
        }

        public ActionResult AddDealer()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddDealer(DealerCodeModel model)
        {
            string status;
            status = _dataAccess.AddHomenetDealerCode(model.DealerCode);

            if(status == "Added")
            {
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.ErrMsg = status;
                return View(model);
            }
                
        }

        public ActionResult ToggleActiveStatus(int accountId, int status)
        {
            var affecteRow = _dataAccess.ChangeHomenetDealerStatus(accountId, status);
            bool success = false;
            string opsStatus = string.Empty;
            if (affecteRow > 0)
            {
                opsStatus = "Action was successful.";
                success = true;
            }
            else
            {
                opsStatus = "Action could not be executed. Please try again or contact your Sys Admin.";
            }
            return Json(new { status = opsStatus, success = success });
        }
       
        public ActionResult RepollDealer(int accountId)
        {
            var affecteRow = _dataAccess.RepollDealer(accountId);
            bool success = false;
            string opsStatus = string.Empty;
            if (affecteRow > 0)
            {
                opsStatus = "Action was successful.";
                success = true;
            }
            else
            {
                opsStatus = "Action could not be executed. Please try again or contact your Sys Admin.";
            }
            return Json(new { status = opsStatus, success = success });
        }
    }
}