﻿using AAA_CSR_Support_Tool.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAA_CSR_Support_Tool.Controllers
{
    public class DealerController : Controller
    {
        private readonly IDataAccess _dataAccess;

        public DealerController(IDataAccess da)
        {
            _dataAccess = da;
        }
        // GET: Dealers
        public ActionResult Index()
        {
            var data = _dataAccess.GetAllDealersFromAAAA();

            return View(data);
        }

        [HttpPost]
        public ActionResult ChangeName(int dealerId, string new_name)
        {
            System.Int32 row;
            row = _dataAccess.ChangeDealerName(dealerId, new_name);
            if(row > 0)
                return Json(new { result = true });
            else
                return Json(new { result = false });
        }

        [HttpPost]
        public ActionResult Undo(int dealerId)
        {
            System.Int32 row;
            row = _dataAccess.UndoChangedName(dealerId);
            if (row > 0)
                return Json(new { result = true });
            else
                return Json(new { result = false });
        }
        
    }
}