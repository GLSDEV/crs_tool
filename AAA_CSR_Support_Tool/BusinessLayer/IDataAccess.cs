﻿using AAA_CSR_Support_Tool.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AAA_CSR_Support_Tool.BusinessLayer
{
    public interface IDataAccess
    {
        IEnumerable<AAADealerModel> GetAllDealersFromAAAA();
        int ChangeDealerName(int dealerId, string newName);
        int UndoChangedName(int dealerId);

        string AddHomenetDealerCode(int Code);

        int ChangeHomenetDealerStatus(int dealerId, int status);

        IEnumerable<HomenetDealerModel> GetAllHomenetDealers();
        int RepollDealer(int AccountId);
    }
}