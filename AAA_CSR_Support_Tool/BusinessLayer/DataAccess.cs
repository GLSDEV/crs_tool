﻿using AAA_CSR_Support_Tool.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AAA_CSR_Support_Tool.BusinessLayer
{
    public class DataAccess : IDataAccess
    {

        private readonly string conStr = ConfigurationManager.ConnectionStrings["DataAccessQuickStart"].ToString();
        private readonly string homentConStr = ConfigurationManager.ConnectionStrings["AAA_InventroyConStr"].ToString();
        SqlConnection connection; 
        public IEnumerable<AAADealerModel> GetAllDealersFromAAAA()
        {
            IList<AAADealerModel> modelList = new List<AAADealerModel>();
            connection = new SqlConnection(conStr);
            using (connection)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "CRS_Tool_DealersGetAll";
                cmd.Connection = connection;
                connection.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var model = new AAADealerModel() {
                            DealerID = reader.SafeGetInt32(0),
                            DealerName = reader.SafeGetString(1),
                            PhoneNo = reader.SafeGetString(2),
                            FaxNo = reader.SafeGetString(3),
                            GeneralManager = reader.SafeGetString(4),
                            IsAAR = reader.SafeGetBool(5),
                        };
                        modelList.Add(model);
                    }
                }

                return modelList;
            }
        }

        public int ChangeDealerName(int dealerId, string newName)
        {
            connection = new SqlConnection(conStr);
            using (connection)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@DealerId", SqlDbType.Int);
                cmd.Parameters["@DealerId"].Value = dealerId;
                cmd.Parameters.Add("@DealerNewName", SqlDbType.VarChar);
                cmd.Parameters["@DealerNewName"].Value = newName;

                cmd.CommandText = "CRS_Tool_CahngeDealerName";
                cmd.Connection = connection;
                connection.Open();

                System.Int32 rowsAffected = cmd.ExecuteNonQuery();
                return rowsAffected;
            }
        }

        public int UndoChangedName(int dealerId)
        {
            connection = new SqlConnection(conStr);
            using (connection)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@DealerId", SqlDbType.Int);
                cmd.Parameters["@DealerId"].Value = dealerId;

                cmd.CommandText = "CRS_Tool_UndoCahngedDealerName";
                cmd.Connection = connection;
                connection.Open();

                System.Int32 rowsAffected = cmd.ExecuteNonQuery();
                return rowsAffected;
            }
        }

        public string AddHomenetDealerCode(int code)
        {
            var status = string.Empty;
            connection = new SqlConnection(conStr);
            using (connection)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ACCOUNT_ID", SqlDbType.Int);
                cmd.Parameters["@ACCOUNT_ID"].Value = code;

                cmd.CommandText = "CRS_Tool_AddHomenetDealer";
                cmd.Connection = connection;
                connection.Open();

                var reader= cmd.ExecuteReader ();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        status = reader.GetString(0);
                    }
                }
                else
                    status = "Unable to find Account";
                return status;
            }
        }

        public IEnumerable<HomenetDealerModel> GetAllHomenetDealers()
        {
            IList<HomenetDealerModel> modelList = new List<HomenetDealerModel>();
            connection = new SqlConnection(homentConStr);
            using (connection)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "CRS_Tool_HomenetDealersGetAll";
                cmd.Connection = connection;
                connection.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var model = new HomenetDealerModel()
                        {
                            DealerId = reader.SafeGetInt32(0),
                            AccountId = reader.SafeGetInt32(1),
                            DealerName = reader.SafeGetString(2),
                            DealerZip = reader.SafeGetString(3),
                            DealerUrl = reader.SafeGetString(4),
                            IsActive = reader.SafeGetBool(5),
                            UpdateDate = reader.SafeGetDate(6)
                        };
                        modelList.Add(model);
                    }
                }

                return modelList;
            }
        }

        public int ChangeHomenetDealerStatus(int dealerId, int status)
        {
            connection = new SqlConnection(homentConStr);
            using (connection)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@AccountId", SqlDbType.Int);
                cmd.Parameters["@AccountId"].Value = dealerId;
                cmd.Parameters.Add("@Status", SqlDbType.Int);
                cmd.Parameters["@Status"].Value = status;

                cmd.CommandText = "CRS_Tool_ChangeHomenetDealerStatus";
                cmd.Connection = connection;
                connection.Open();

                return cmd.ExecuteNonQuery(); ;
            }
        }

        public int RepollDealer(int dealerId)
        {
            connection = new SqlConnection(homentConStr);
            using (connection)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@DealerId", SqlDbType.Int);
                cmd.Parameters["@AccountId"].Value = dealerId;

                cmd.CommandText = "CRS_Tool_RepollDealer";
                cmd.Connection = connection;
                connection.Open();

                return cmd.ExecuteNonQuery(); ;
            }
        }
    }
}