﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AAA_CSR_Support_Tool.Startup))]
namespace AAA_CSR_Support_Tool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
