﻿using System.Web;
using System.Web.Mvc;

namespace AAA_CSR_Support_Tool
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
