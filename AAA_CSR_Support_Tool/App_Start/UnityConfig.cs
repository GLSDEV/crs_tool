using AAA_CSR_Support_Tool.BusinessLayer;
using AAA_CSR_Support_Tool.Controllers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Mvc;
using Unity;
using Unity.Injection;
using Unity.Mvc5;

namespace AAA_CSR_Support_Tool
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<AccountController>(new InjectionConstructor());
            container.RegisterType(typeof(IUserStore<>), typeof(UserStore<>));
            container.RegisterType<IDataAccess, DataAccess>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}