﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAA_CSR_Support_Tool.Models
{
    public class AAADealerModel
    {
        public int DealerID { set; get; }
        public string DealerName { set; get; }
        public string PhoneNo { set; get; }
        public string FaxNo { set; get; }
        public string GeneralManager { set; get; }
        public bool IsAAR { set; get; }
    }
}