﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AAA_CSR_Support_Tool.Models
{
    public class HomenetDealerModel
    {
        public int DealerId { set; get; }
        [DisplayName("Account ID")]
        public int AccountId { set; get; }
        [DisplayName("Dealer Name")]
        public string DealerName { set; get; }

        [DisplayName("Zip")]
        public string DealerZip { set; get; }

        [DisplayName("Dealer Website")]
        public string DealerUrl { set; get; }

        [DisplayName("Activated?")]
        public bool IsActive { set; get; }

        [DisplayName("Update Date")]
        public DateTime UpdateDate { set; get; }
    }
}