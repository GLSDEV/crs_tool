﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AAA_CSR_Support_Tool.Models
{
    public class DealerCodeModel
    {
        [Required]
        [RegularExpression(@"^\d+$", ErrorMessage = "Please, only numbers are allowed")]
        [DisplayName("Dealer Code")]
        public int DealerCode { set; get; }
    }
}